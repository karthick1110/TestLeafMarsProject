package wdMethods;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtendReport {
	
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testCaseName, testCaseDescription,author,category,fileName;

	public void startReport() {
		ExtentHtmlReporter htmlObject = new ExtentHtmlReporter("./reports/executionreport.html");
		htmlObject.setAppendExisting(false);
		extent = new ExtentReports();
		extent.attachReporter(htmlObject);

	}
	
	public void createTestCaseHeaders() {
		test = extent.createTest(testCaseName, testCaseDescription);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	public void reportTestSteps(String Description, String Status) {
		try {
			if (Status.equalsIgnoreCase("pass")) {
			test.pass(Description);
			}else if (Status.equalsIgnoreCase("fail")){
				test.fail(Description);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public  void writeReport() {
			extent.flush();
			System.out.println("Complete");
			
	}

}
