package wdMethods;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import util.ReadExcel;


public class ProjectMethods extends SeMethods{
	
	@DataProvider(name="fetchData")
	public Object[][] getData() throws InvalidFormatException, IOException {
		Object[][] data1 = ReadExcel.getData(fileName);
		
		return data1;
	}

	@BeforeSuite(groups="common")
	public void beforeSuite() {
		startReport();
	}
	
	
	@BeforeMethod(groups="common")
	@Parameters({"browser","userName","passWord","url"})
	public void login(String bName, String uName, String pwd, String url) {
		createTestCaseHeaders();
		startApp(bName, url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uName);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
	}
	@AfterMethod(groups="common")
	public void closeBrowser() {
		driver.close();
	}
	
	@AfterSuite(groups="common")
	public void endResult() {
		writeReport();
	}

}








