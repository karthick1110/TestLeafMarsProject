package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods extends ExtendReport implements WdMethods{

	public int i = 1;
	public RemoteWebDriver driver;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-notifications");
				driver = new ChromeDriver(options);
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "linkText" : return driver.findElementByLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		} catch (Exception e) {
			System.err.println("Unknow Exception ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		/*	WebElement findElementByLinkText = driver.findElementByLinkText(locValue);
		return findElementByLinkText;*/
		return null;
	}


	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportTestSteps("The data "+data+" is Entered Successfully", "pass");
		} catch (WebDriverException e) {
			reportTestSteps("The data "+data+" is not Entered Successfully", "fail");
		} finally {
			takeSnap();
		}
	}


	public void clickWithNoSnap(WebElement ele) {
		String text = ele.getText();
		if (text=="") {
			text = ele.getAttribute("id");

		}String tagClick = ele.getTagName();
		try {
			ele.click();
			reportTestSteps("Element Text : \"" + text + " Tag Name : " + tagClick + "\" clicked successfully ", "pass");
		} catch (WebDriverException e) {
			reportTestSteps("Element Text : \"" + text + " Tag Name : " + tagClick + "\" not clicked successfully ", "fail");
		}
	}

	public void click(WebElement ele) {
		String text = ele.getText();
		String tagClick = ele.getTagName();
		try {

			ele.click();
			reportTestSteps("clicked successfully ", "pass");
		} catch (WebDriverException e) {
			reportTestSteps("Element Text : \"" + text + " Tag Name : " + tagClick + "\" not clicked successfully ", "fail");
		} finally {
			takeSnap();
		}
	}

	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			reportTestSteps("The DropDown Is Selected with VisibleText "+value, "pass");
		} catch (Exception e) {
			reportTestSteps("The DropDown is not selected with visibleText "+value, "fail");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {

		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			reportTestSteps("The DropDown "+ ele.getAttribute("name") + " is selected using Index :"+index, "pass");
		} catch (Exception e) {
			reportTestSteps("The DropDown "+ ele.getAttribute("name") + " is not selected using Index : "+index, "fail");
		} finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		try {
			if (driver.getTitle().contains(expectedTitle)) {
				reportTestSteps("Title : " + driver.getTitle() + " matches Expected Result : ", "pass");
				return true;
			}else {
				reportTestSteps("Title : " + driver.getTitle() + " does not match Expected Result : " + expectedTitle, "fail");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			takeSnap();
		}
		return false;

	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			if(ele.getText().equals(expectedText)) {
				reportTestSteps("Input " + expectedText + " matches the WebElement Text : " + ele.getText(), "pass");
			}
			else {
				reportTestSteps("Input " +expectedText + "does not Match the WebElement Text : " + ele.getText(), "fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {takeSnap();}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			if (ele.getText().contains(expectedText)) {
				reportTestSteps("Web Element Text contains the given text : " + expectedText, "pass");
			}
			else {
				reportTestSteps("Web Element Text does not contain the given text : " + expectedText, "fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {takeSnap();}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			if(ele.getAttribute(attribute).equals(value)) {
				reportTestSteps("Web Element  matches the given Attribute : " + attribute, "pass");
			}
			else {
				reportTestSteps("Web Element does not match the given Attribute : " + attribute, "fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {takeSnap();}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			if(ele.getAttribute(attribute).contains(value)) {
				System.out.println("Web Element contains the given Attribute : " + attribute);
			}
			else {
				System.out.println("Web Element does not contains the given Attribute : " + attribute);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {takeSnap();}

	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				System.out.println("Web Element :" + ele + " is selected");
			}else {
				System.out.println("Web Element :" + ele + " is not selected");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {takeSnap();}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed()) {
				System.out.println("Web Element :" + ele + " is displayed");
			}else {
				System.out.println("Web Element :" + ele + " is not displayed");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {takeSnap();}

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> listOfWindows =  new ArrayList<String>();
		listOfWindows.addAll(windowHandles);
		driver.switchTo().window(listOfWindows.get(index));
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
	}

	@Override
	public String getAlertText() {
		String text = driver.switchTo().alert().getText();
		return text;
	}


	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	public String GetTableColumnText(WebElement ele,int rows,int column)  {
		String out = null;
		try {
			List<WebElement> listoftr = ele.findElements(By.tagName("tr"));
			List<WebElement> columns = listoftr.get(rows).findElements(By.tagName("td"));
			out = columns.get(column).getText();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {driver.quit();}

}
