package week3day2classwork;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ErailTableList {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.get("https://erail.in");
		
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		
		boolean selectedCheck = driver.findElementById("chkSelectDateOnly").isSelected();
		
		if(selectedCheck==true) {
			driver.findElementById("chkSelectDateOnly").click();
			
			WebElement TrainListTable = driver.findElementByXPath("//table[@class='DataTable TrainList']");
			List<WebElement> Rows = TrainListTable.findElements(By.tagName("tr"));
			
			for (WebElement eachwebElem : Rows) {
				WebElement td = eachwebElem;
				List<WebElement> columns = td.findElements(By.tagName("td"));
				System.out.println(columns.get(1).getText());
				
				
			}
			
		//	WebElement td = Rows.get(0);
			
			
			
			
		}
		
	}

}
