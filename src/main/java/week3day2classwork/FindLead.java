package week3day2classwork;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FindLead {

	public static void main(String[] args) throws IOException, InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		// Get url
		driver.get("http://leaftaps.com/opentaps");
		// Login Details
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();

		// Click on the CRM/FMA link

		driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
		driver.findElementByXPath("//a[contains(text(),'Leads')]").click();
		driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Karthick");
		driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("Murugiah");

		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();

		//WebDRiver wait
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//table[@class='x-grid3-row-table'])[1]//a[1]")));
		driver.findElementByXPath("(//table[@class='x-grid3-row-table'])[1]//a[1]").click();
		// Screenshots
		File screenshotAs = driver.getScreenshotAs(OutputType.FILE);
		File destination = new File("./Snaps/image.png");
		FileUtils.copyFile(screenshotAs, destination);

		Thread.sleep(3000);
		driver.close();
		System.out.println("Completed");
	}

}
