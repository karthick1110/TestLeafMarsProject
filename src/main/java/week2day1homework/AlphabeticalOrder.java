package week2day1homework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class AlphabeticalOrder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String input = "amazon development center";
		List<Character> setchar = new ArrayList<>(); 
		char charArray[] = input.toCharArray();

		for (char cChar : charArray) {
			setchar.add(cChar);
		}

		Collections.sort(setchar);

		for (char c : setchar) {

			if(c!=' ') {
				System.out.println(c);
			}

		}
	}

}
