package week2day1homework;

import java.util.LinkedHashSet;
import java.util.Set;

public class PrintUniqueCharacters {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Set<Character> setChar = new LinkedHashSet<Character>();

		String input = "cognizat india";

		char charArray[] = input.toCharArray();

		for (char c : charArray) {
			setChar.add(c);
		}

	for (char c : setChar) {
		System.out.println(c);
	}	
	}

}
