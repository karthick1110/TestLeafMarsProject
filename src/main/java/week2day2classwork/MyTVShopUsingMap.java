package week2day2classwork;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MyTVShopUsingMap {

	public static void main(String[] args) {
		int totalTVs =0;
		int lastModelTvCount = 0;
		Map<String, Integer> tvCount = new LinkedHashMap<String, Integer>();

		tvCount.put("Sony", 5);
		tvCount.put("Samsung", 10);
		tvCount.put("Onida", 2);
		tvCount.put("MI", 4);

		//System.out.println(tvCount.size());

		for (Entry<String, Integer> tv : tvCount.entrySet()) {
			totalTVs = totalTVs + tv.getValue();
			lastModelTvCount = tv.getValue();

		}
		System.out.println("Total TV count : " + totalTVs);
		System.out.println("Last Model TV value : " + lastModelTvCount);

		tvCount.put("Sony", 3);
		System.out.println("Sony TV Latest Count : " + tvCount.get("Sony"));
		
		//System.out.println(tvCount);
		
		// Get the second tv value
		Set<String> keySet = tvCount.keySet();
		System.out.println("Key Size : " + keySet.size());
		
		List<String> keyList = new ArrayList<String>();
		keyList.addAll(keySet);
		
		System.out.println(keyList.get(2));
		
		System.out.println(tvCount.get(keyList.get(2)));
		
		try {
			System.out.println(keyList.get(6));
		} catch (IndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			System.out.println(" KeyList does not contain the given Element");
		}
		

	}

}
