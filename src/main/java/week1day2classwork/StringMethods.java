package week1day2classwork;

public class StringMethods {
	
	public static void main(String[] args) {
		
		String name = "KARthick";
		
		//to Lowercase
		System.out.println(name.toLowerCase());
		
		// Find Length
		
		System.out.println(name.length());
		
		// find 3rd element of the name
		
		System.out.println(name.indexOf('A'));
		
		// Concatenate
		
		System.out.println(name.concat(" M"));
		
		//equals
		
		System.out.println(name.equals("KArthick"));
		
		// equals ignorecase
		
		System.out.println(name.equalsIgnoreCase("KARTHICK"));
	}

}
