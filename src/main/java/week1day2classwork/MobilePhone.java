package week1day2classwork;

import java.util.Scanner;

public class MobilePhone {

	static long IMEINumber;
	String Make;
	float Price;
	int pixel;
	boolean isDualSimEnabled;
	char Warranty;
	static int MobileNumber;

	static boolean lockThePhone(long IMEIToLock){
		IMEINumber = IMEIToLock;
		boolean PhoneStatus = true;
		System.out.println(IMEINumber + " : Phone is locked");
		return PhoneStatus;

	}

	static void addContact(int MobileNum){
		MobileNumber = MobileNum;
		System.out.println("Mobile Number " + MobileNumber + " added successfully");
	}
	static void call(int MobileNumber){
		System.out.println("Calling the Mobile Number " + MobileNumber);
	}

	static void phoneSpecification(long IMEINumber,String Make,float Price,int pixel, boolean isDualSimEnabled, char Warranty) {

		System.out.println("Make : " + Make);
		System.out.println("Price : " + Price);
		System.out.println("Camera  : " + pixel + "MP");
		System.out.println("Dual SIM Enabled : " + isDualSimEnabled);
		System.out.println("Waranty : " + Warranty);
		System.out.println(Make + " IMEI Numer : "+ IMEINumber + "\n");
	}

	static void sendSMS(long RMobileNumber, String Text) {
		System.out.println("MSG : " + Text + ", Has been sent to Recepient : " + RMobileNumber);

	}
	
	void sendCall(long RMobileNumber) {
		System.out.println("Call to the Number : " + RMobileNumber + " is complete.");

	}

	static void priceCompare(int price1, String Make1, int price2, String Make2) {

		if (price1>price2) {
			System.out.println(Make1 + " phone is having greater price " + price1) ;
		}else {
			System.out.println(Make2 + " phone is having greater price " + price2) ;
		}

	}


	static void checkMobileServiceProvider(int SPCode) {
		
		switch (SPCode) {
		
		case 944 :
			System.out.println("Provider : BSNL");
			break;
		case 897 :
			System.out.println("Provider : Idea");
			break;
		case 630:
			System.out.println("Provider : JIO");
			break;
		case 900:
			System.out.println("Provider : Airtel");
			break;
		default:
			System.out.println("Please Enter valid code : 944,900,630");
		}
		
		/*
		
		if(SPCode == 944) {
			
		}else if (SPCode == 900) {
			
		}else if (SPCode == 897) {
			
		}else if (SPCode == 630) {
			
		}else {
			
		}
*/		
	}
	public static void main(String[] args) {

		phoneSpecification(895623568989l, "Vivo", 20000.99f, 24, true, 'Y');
		addContact(967787);
		boolean phonestatus = lockThePhone(20508502689343l);
		call(979898343);
		System.out.println("Phone Status : " + phonestatus);

		sendSMS(967787447, "Hello Buddy");
		sendSMS(9000812341l, "Good morning!Happy Sunday!");
		sendSMS(9000812342l, "Good morning!Happy Sunday!");
		sendSMS(9000812343l, "Good morning!Happy Sunday!");	
		sendSMS(9000812344l, "Good morning!Happy Sunday!");
		sendSMS(9000812345l, "Good morning!Happy Sunday!");
		sendSMS(9000812346l, "Good morning!Happy Sunday!");


		priceCompare(4000, "Samsung", 6000, "Nokia");
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Please Enter First 3 Digit of your Mobile Number to find service provier : ");
		int SPCode = scan.nextInt();
		checkMobileServiceProvider(SPCode);
		
		scan.close();
	}


}
