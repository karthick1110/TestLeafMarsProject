package week4day1classwork;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandleAlertsAndFrames {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		// Enter in to the Frame
		
		driver.switchTo().frame("iframeResult");
		
		driver.findElement(By.xpath("//button[text()='Try it']")).click();
		
		//Handle Alert
		
		driver.switchTo().alert().sendKeys("Karthick");
		driver.switchTo().alert().accept();
				
		//
		 String text = driver.findElement(By.id("demo")).getText();
		 System.out.println(text);
		 driver.switchTo().defaultContent();
	}

}
