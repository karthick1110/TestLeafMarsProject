package week4day1classwork;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;


public class HandleMultipleWindows {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.findElementByLinkText("AGENT LOGIN").click();
		String firstwindowHandleId = driver.getWindowHandle();
		System.out.println(firstwindowHandleId);
		driver.findElementByLinkText("Contact Us").click();

		Set<String> setWindowHandles = driver.getWindowHandles();
		System.out.println("No of Windows " + setWindowHandles.size());

		List<String> listofWindows = new ArrayList<String>();
		listofWindows.addAll(setWindowHandles);
		driver.switchTo().window(listofWindows.get(1));
		System.out.println("Current URL : " + driver.getCurrentUrl());
		System.out.println("Title of Current Window : " + driver.getTitle());
		driver.quit();

	}

}
