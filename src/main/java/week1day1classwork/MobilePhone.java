package week1day1classwork;

import week1day1homework.Datatypesclass;

public class MobilePhone {

	static long IMEINumber;
	String Make;
	float Price;
	int pixel;
	boolean isDualSimEnabled;
	char Warranty;
	static int MobileNumber;

	public static boolean lockThePhone(long IMEIToLock){
		long IMEINumber = IMEIToLock;
		boolean PhoneStatus = true;
		System.out.println(IMEINumber + " : Phone is locked");
		return PhoneStatus;
	}
	
	
	public Datatypesclass SetText() {
		return new Datatypesclass();
	}
	
	

	static void addContact(int MobileNum){
		MobileNumber = MobileNum;
		System.out.println("Mobile Number " + MobileNumber + " added successfully");
	}
	static void call(int MobileNumber){
		System.out.println("Calling the Mobile Number " + MobileNumber);
	}
	
	static void phoneSpecification(long IMEINumber,String Make,float Price,int pixel, boolean isDualSimEnabled, char Warranty) {
				
		System.out.println("Make : " + Make);
		System.out.println("Price : " + Price);
		System.out.println("Camera  : " + pixel + "MP");
		System.out.println("Dual SIM Enabled : " + isDualSimEnabled);
		System.out.println("Waranty : " + Warranty);
		System.out.println(Make + " IMEI Numer : "+ IMEINumber + "\n");
	}
	
	static void sendSMS(int RMobileNumber, String Text) {
		System.out.println("MSG : " + Text + ", Has been sent to Recepient : " + RMobileNumber);

	}
	static void deleteContact(long MobileNumber) {
		System.out.println("Mobile Number : " + MobileNumber + " has been deleted");
	}

	public static void main(String[] args) {
		
		phoneSpecification(895623568989l, "Vivo", 20000.99f, 24, true, 'Y');
		addContact(967787);
		boolean phonestatus = lockThePhone(20508502689343l);
		call(979898343);
		System.out.println("Phone Status : " + phonestatus);
		
		sendSMS(967787447, "Hello Buddy");
		
		deleteContact(9677874477l);
	}


}
