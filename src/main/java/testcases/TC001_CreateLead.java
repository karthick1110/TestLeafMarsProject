package testcases;




import org.openqa.selenium.WebElement;
import org.testng.annotations.*;


import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeClass(groups="common")
	public void TestData() {
		testCaseName="TC_001";
		testCaseDescription="Create Lead";
		author = "Karthick";
		category="Smoke";
		fileName = "createlead";
	}
	@Test(/*groups= {"sanity","smoke"}, */dataProvider="fetchData")
	public void Steps(String cName, String FName,String LName,String email,String Ph) {

		//login();
		// Step 5
		WebElement crmSfaLink = locateElement("linkText", "CRM/SFA");
		click(crmSfaLink);
		// Step 6 Click Create Lead
		WebElement eleCreateLead = locateElement("linkText", "Create Lead");
		click(eleCreateLead);
		// Step 7 Enter Company Name
		WebElement eleCompNameTB = locateElement("id", "createLeadForm_companyName");
		type(eleCompNameTB, cName);
		// Step 8 Enter First Name
		WebElement eleFirstNameTB = locateElement("id","createLeadForm_firstName");
		type(eleFirstNameTB, FName);
		// Step 9 Enter Last Name
		WebElement eleLastNameTB = locateElement("id","createLeadForm_lastName");
		type(eleLastNameTB, LName);
		// Step 10 Choose Source
		WebElement eleSourceDd = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSourceDd, "Conference");
		// Step 11 Choose Marketing Campaign
		WebElement eleMarketingCamp = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(eleMarketingCamp, 1);
		// Step 12 Enter phone number
		WebElement elePhoneNumber = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(elePhoneNumber, Ph);
		// Step 13 Enter email address
		WebElement eleEmailAddress = locateElement("id", "createLeadForm_primaryEmail");
		type(eleEmailAddress, email);
		// Step 14 Click Create lead
		WebElement eleCreateLeadButton = locateElement("class", "smallSubmit");
		click(eleCreateLeadButton);
		// Step 15 Verify the first name
		WebElement eleFirstNameV = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleFirstNameV, "Karthick");
		
	
	}
	
	

}
