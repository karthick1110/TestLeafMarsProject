package testcases;

import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNgAnnotations extends ProjectMethods{
  @Test
  public void f() {
	  System.out.println("    @Test Method");
  }

  @BeforeClass
  public void beforeClass() { System.out.println("@Before Class");
  }

  @AfterClass
  public void afterClass() { System.out.println("@AfterClass");
  }

  @BeforeTest
  public void beforeTest() {System.out.println("@BeforeTest");
  }

  @AfterTest
  public void afterTest() { System.out.println("@AfterTest");
  }

  @BeforeSuite
  public void beforeSuite() { System.out.println("@Before Suite");
  }

  @AfterSuite
  public void afterSuite() { System.out.println("@After Suite");
  }

}
