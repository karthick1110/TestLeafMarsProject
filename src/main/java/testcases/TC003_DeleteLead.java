package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods{
	@BeforeClass
	public void TestData() {
		testCaseName="TC_003";
		testCaseDescription="Delete Lead";
		author = "Karthick";
		category="Smoke";
	}
	@Test(dependsOnMethods="testcases.TC002_EditLead.editLeadSteps",groups="smoke")
		public void DeleteLeadSteps() {
		//login();
		// Step 5 Click crm/sfa link
		WebElement crmSfaLink = locateElement("linkText", "CRM/SFA");
		click(crmSfaLink);
		//Step 6 Click Leads link
		WebElement eleLeads = locateElement("linkText", "Leads");
		click(eleLeads);
	}
}
