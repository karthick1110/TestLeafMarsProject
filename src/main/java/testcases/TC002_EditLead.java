package testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;



public class TC002_EditLead extends ProjectMethods{
	@BeforeClass(groups="common")
	public void TestData() {
		testCaseName="TC_002";
		testCaseDescription="Edit Lead";
		author = "Karthick";
		category="Smoke";
		fileName="editlead";
	}
	@Test(/*dependsOnMethods="testcases.TC001_CreateLead.Steps",*/groups= {"smoke","Regression"},dataProvider="fetchData")
	public void editLeadSteps(String fName,String lName) {
		try {
			//login();
			// Step 5 Click crm/sfa link
			WebElement crmSfaLink = locateElement("linkText", "CRM/SFA");
			click(crmSfaLink);
			//Step 6 Click Leads link
			WebElement eleLeads = locateElement("linkText", "Leads");
			click(eleLeads);
			// Step 7 Click Find leads
			WebElement eleFindLeads = locateElement("linkText", "Find Leads");
			click(eleFindLeads);
			//Step 8 Enter first name
			WebElement eleFirstnameTB = locateElement("xpath","(//input[@name='firstName'])[3]");
			type(eleFirstnameTB, fName);
			WebElement elemetLastName = locateElement("xpath", "(//input[@name='lastName'])[3]");
			type(elemetLastName,lName);
			// Step 9 Click Find leads button
			WebElement eleFindButton = locateElement("xpath","(//button[text()='Find Leads'])");
			click(eleFindButton);
			// Step 10 Click on first resulting lead
			WebElement eleTable = locateElement("xpath", "//table[@class='x-grid3-row-table']");
			String eleText = GetTableColumnText(eleTable, 0, 0);
			WebElement clickTableData = locateElement("linkText",eleText);
			click(clickTableData);
			// Step 11 Verify title of the page
			verifyTitle("View Lead | opentaps CRM");
			// Step 12 Click Edit
			WebElement eleEditButton = locateElement("linkText", "Edit");
			click(eleEditButton);
			// Step 13 Change the company name
			
			// Step 14 Click Update

			// Step 15 Confirm the changed name appears

						

		} catch (Exception e) {
			driver.quit();
		}
				

	}
	
	@DataProvider
	public Object[][] getData1() {
		Object[][] data = new Object[2][2];
		data[0][0]="Karthick";
		data[0][1]="M";
		data[1][0]="Karthick";
		data[1][1]="Murugiah";
		return data;
	}
}
