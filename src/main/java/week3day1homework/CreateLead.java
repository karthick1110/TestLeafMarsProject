package week3day1homework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);

		// Open the URL
		driver.get("http://leaftaps.com/opentaps");				
		// Login Details
		driver.findElementById("username").sendKeys("DemoSalesManager");	
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		// Page Navigations
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		// Fill in the Create Lead form
		// Text Box

		try {
			driver.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
			driver.findElementById("createLeadForm_parentPartyId").sendKeys("Company");
			driver.findElementById("createLeadForm_firstName").sendKeys("Karthick");
			driver.findElementById("createLeadForm_lastName").sendKeys("Murugiah");
			driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Karthi");
			driver.findElementById("createLeadForm_lastNameLocal").sendKeys("M");
			driver.findElementById("createLeadForm_personalTitle").sendKeys("Hi");
			driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mr.");
			driver.findElementById("createLeadForm_departmentName").sendKeys("Information Technology");
			driver.findElementById("createLeadForm_annualRevenue").sendKeys("0000010");
			driver.findElementById("createLeadForm_numberEmployees").sendKeys("100");
			driver.findElementById("createLeadForm_sicCode").sendKeys("1110");
			driver.findElementById("createLeadForm_tickerSymbol").sendKeys("NASDAQ:AAPL");
			driver.findElementById("createLeadForm_description").sendKeys("Hi Hello Welcome");
			driver.findElementById("createLeadForm_importantNote").sendKeys(":) This is important Note..!!");
			//Contact information Text Box
			driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
			driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
			driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
			driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("2341234");
			driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("5555");
			driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Xyz");
			driver.findElementById("createLeadForm_primaryEmail").sendKeys("abc@abc.com");
			driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://leaftaps.com/");
			// Primary Address  Text Box
			driver.findElementById("createLeadForm_generalToName").sendKeys("Karthick M");
			driver.findElementById("createLeadForm_generalAttnName").sendKeys("Karthi");
			driver.findElementById("createLeadForm_generalAddress1").sendKeys("Vivekanandar ST");
			driver.findElementById("createLeadForm_generalAddress2").sendKeys("Dubai Main Road");
			driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
			driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600001");
			driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("001");

		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.out.println("Element Not Found" + e.getMessage());
		}
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		WebElement marketingCampaign  = driver.findElementById("createLeadForm_marketingCampaignId");
		WebElement preferredCurrency = driver.findElementById("createLeadForm_currencyUomId");
		WebElement Industry = driver.findElementById("createLeadForm_industryEnumId");
		WebElement Ownership = driver.findElementById("createLeadForm_ownershipEnumId");
		// Address Drop down box
		WebElement Country = driver.findElementById("createLeadForm_generalCountryGeoId");
		WebElement State = driver.findElementById("createLeadForm_generalStateProvinceGeoId");

		// Object Creation for Select Class
		Select objSource = new Select(source);
		Select objMarketingCampaign = new Select(marketingCampaign);
		Select objPreferredCurrency = new Select(preferredCurrency);
		Select objIndustry = new Select(Industry);
		Select objOwnership = new Select(Ownership);
		Select objCountry = new Select(Country);
		Select objState = new Select(State);

		// Choose Drop down box values

		objSource.selectByValue("LEAD_EMPLOYEE");
		objMarketingCampaign.selectByValue("CATRQ_AUTOMOBILE");
		objPreferredCurrency.selectByVisibleText("INR - Indian Rupee");
		objIndustry.selectByValue("IND_NON_PROFIT");
		objOwnership.selectByValue("OWN_PUBLIC_CORP");

		objCountry.selectByVisibleText("India");
		Thread.sleep(3000);
		objState.selectByVisibleText("TAMILNADU");

		// Create Lead

		driver.findElementByClassName("smallSubmit").click();
		Thread.sleep(3000);
		System.out.println("Lead Created Successfully..!!");

		driver.close();
	}
}
