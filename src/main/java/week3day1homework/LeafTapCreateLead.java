package week3day1homework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LeafTapCreateLead {

	public static void main(String[] args)  {

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
		// Open the URL
		driver.get("http://leaftaps.com/opentaps");				
		// Login Details
		driver.findElementById("username").sendKeys("DemoSalesManager");	
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		// Page Navigations
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		// Fill in the Create Lead form

		// Text Box
	
			driver.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
			driver.findElementById("createLeadForm_parentPartyId").sendKeys("Company");
			driver.findElementById("createLeadForm_firstName").sendKeys("Karthick");
			driver.findElementById("createLeadForm_lastName").sendKeys("Murugiah");
			driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Karthi");
			driver.findElementById("createLeadForm_lastNameLocal").sendKeys("M");
			driver.findElementById("createLeadForm_personalTitle").sendKeys("Hi");
			driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mr.");
			driver.findElementById("createLeadForm_departmentName").sendKeys("Information Technology");
			driver.findElementById("createLeadForm_annualRevenue").sendKeys("0000010");
			driver.findElementById("createLeadForm_numberEmployees").sendKeys("100");
			driver.findElementById("createLeadForm_sicCode").sendKeys("1110");
			driver.findElementById("createLeadForm_tickerSymbol").sendKeys("NASDAQ:AAPL\r\n");
		
			
		
	//	driver.close();
	}

}
