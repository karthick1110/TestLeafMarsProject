package codingchallenge;

import java.util.Scanner;

public class DailyChallenge3Palindrome250718 {

	public static void main(String[] args) {
		
		        int n, m, a = 0,x;
		        Scanner s = new Scanner(System.in);
		        System.out.print("Enter any number:");
		        n = s.nextInt();
		        m = n;
		        while(n > 0)
		        {
		        	System.out.print(n);
		            x = n % 10;
		            //System.out.println( " X Value (n%10) : "+ x);
		            System.out.print(a);
		            a = a * 10 + x;
		            //System.out.println( " A Value (a*10+x) : "+ a);
		            System.out.print(n);
		            n = n / 10;
		            //System.out.println( " N Value (n/10) : "+ n);
		        }
		        if(a == m)
		        {
		            System.out.println("Given number "+m+" is Palindrome");
		        }
		        else
		        {
		            System.out.println("Given number "+m+" is Not Palindrome");
	
		        }
		        s.close();
	}

}
