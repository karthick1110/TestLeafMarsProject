package codingchallenge;

import java.util.Scanner;

// Write a java program to find the largest of Three given numbers

public class DailyChallenge2Largest3Nums240718 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.print("Enter a Number1: ");
		int n1=scan.nextInt();
		System.out.print("Enter a Number2: ");
		int n2= scan.nextInt();
		System.out.print("Enter a Number3: ");
		int n3 = scan.nextInt();
		int largest=0;

		if(n1>n2) {	largest = n1;} else {largest = n2;}

		if(largest<n3) { largest =n3;}

		System.out.println("Largest number is :" + largest);
		scan.close();
	}
}
