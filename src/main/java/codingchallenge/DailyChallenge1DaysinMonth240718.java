package codingchallenge;

import java.util.Scanner;

//Write a simple java program to determine the month based on the given number and find the number of days in the Month.

public class DailyChallenge1DaysinMonth240718 {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter the Month to find the no of days");
		int n1=scan.nextInt();
		
		switch (n1){
		case 1:
			System.out.println("Days :" + 31);
			break;
		case 2:
			System.out.println("Days :" + 28);
			break;
		case 3:
			System.out.println("Days :" + 31);
			break;
		case 4:
			System.out.println("Days :" + 30);
			break;
		case 5:
			System.out.println("Days :" + 31);
			break;
		case 6:
			System.out.println("Days :" + 30);
			break;
		case 7:
			System.out.println("Days :" + 31);
			break;
		case 8:
			System.out.println("Days :" + 31);
			break;
		case 9:
			System.out.println("Days :" + 30);
			break;
		case 10:
			System.out.println("Days :" + 31);
			break;
		case 11:
			System.out.println("Days :" + 30);
			break;
		case 12:
			System.out.println("Days :" + 31);
			break;
		default:
			System.out.println(" Please enter the valid month from 1 to 12" );
		} // Switch end
		
		
		scan.close();
	}


}//end
