package week1day1homework;

import java.util.Scanner;

public class ArithmaticOperators {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Please Enter the input n1 : ");
		int n1 = scan.nextInt();
		System.out.println("Please Enter the input n2 : ");
		int n2 = scan.nextInt();

		// + Operator
		int c= n1+n2;
		System.out.println("Arithmatic Operator : + : " + c);
		// - Operator
		c = n1-n2;
		System.out.println("Arithmatic Operator : - : " + c);
		// / Divide Operator
		c = n1/n2;
		System.out.println("Arithmatic Operator : / : " + c);

		// * Multiplication Operator
		c = n1*n2;
		System.out.println("Arithmatic Operator : * : " + c);		

		// % Modulo operator
		c=n1%n2;
		System.out.println("Arithmatic Operator : % : " + c);

		Datatypesclass ObjName = new Datatypesclass();
		System.out.println(ObjName.Integervalue);
		scan.close();
	}

}
