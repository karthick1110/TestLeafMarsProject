package week1day1homework;

import week1day1classwork.*;

public class LearningObjects {

	public static void main(String[] args) {
		
		// Creating objects inside the class
		Datatypesclass obj1 = new Datatypesclass();
		
		System.out.println(obj1.Integervalue);
		
		// Creating objects for class outside the package
		
		//MobilePhone obj2 = new MobilePhone();
		MobilePhone.lockThePhone(9856565656l);

		
	}
	
}
