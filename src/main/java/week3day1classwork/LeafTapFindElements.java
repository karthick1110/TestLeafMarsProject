package week3day1classwork;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LeafTapFindElements {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps");
		
		// Get the hyperlinks available on the Login Page
		
		List<WebElement> allLinks = driver.findElementsByTagName("a");
		
		System.out.println(allLinks.size());
		
		for (WebElement eachLink : allLinks) {
			System.out.println(eachLink.getText());
		}
		
		allLinks.get(3).click();
		
		driver.close();
	}
}
