package week3day1classwork;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LeafTapLogin {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps");

		/*// Get the hyperlinks available on the Login Page

		List<WebElement> allLinks = driver.findElementsByTagName("a");

		System.out.println(allLinks.size());

		for (WebElement eachLink : allLinks) {
			System.out.println(eachLink.getText());
		}

		allLinks.get(1).click();*/

		// Login Details

		driver.findElementById("username").sendKeys("DemoSalesManager");	
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();

		// Page Navigations

		driver.findElementByLinkText("CRM/SFA").click();

		driver.findElementByLinkText("Create Lead").click();

		// Fill in the Create Lead form 10268

		driver.findElementById("createLeadForm_companyName").sendKeys("SathiyaK&Co");
		driver.findElementById("createLeadForm_firstName").sendKeys("Sathiya");
		driver.findElementById("createLeadForm_lastName").sendKeys("Kaliyamoorthy");


		// Dropdown Box

		WebElement dElement1 = driver.findElementById("createLeadForm_dataSourceId");

		Select dropDown1 = new Select(dElement1);

		dropDown1.selectByVisibleText("Conference");

		WebElement dElement2 = driver.findElementById("createLeadForm_marketingCampaignId");

		Select dropDown2 = new Select(dElement2);

		dropDown2.selectByValue("CATRQ_AUTOMOBILE");

		WebElement dElement3 = driver.findElementById("createLeadForm_industryEnumId");
		Select dropDown3 = new Select(dElement3);

		List<WebElement> drop3Lists = dropDown3.getOptions();

		dropDown3.selectByIndex(drop3Lists.size()-1);

		for (WebElement eachDrop3List : drop3Lists) {
			System.out.println(eachDrop3List.getText());
		}

		// Click on the create Lead Button

		driver.findElementByClassName("smallSubmit").click();

		System.out.println(driver.getSessionStorage());


	}

}
