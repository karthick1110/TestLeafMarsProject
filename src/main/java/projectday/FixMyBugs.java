package projectday;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class FixMyBugs {

	@Test
	public void Test1() throws InterruptedException {

		// launch the browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.myntra.com/");

		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();


		// Find the count of Jackets
		String leftCount = 
				driver.findElementByXPath("//input[@value='Jackets']/following-sibling::span")
				.getText()
				.replaceAll("\\D", "");
		System.out.println(leftCount);


		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time

			Thread.sleep(5000);


		// Check the count
		String rightCount = 
				driver.findElementByXPath("//h1[text()='Mens Jackets']/following-sibling::span")
				.getText()
				.replaceAll("\\D", "");
		System.out.println(rightCount);

		// If both count matches, say success
		if(leftCount.equals(rightCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		driver.findElementByXPath("//h4[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<Integer> onlyPrice = new ArrayList<Integer>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(Integer.parseInt(productPrice.getText().replaceAll("\\D", "")));
		}
		//System.out.println("size : " + onlyPrice.size());
		// Loop to sysout all values.
		/*for (Integer integer : onlyPrice) {
			System.out.println(integer);
		}*/
		// Sort them 
		Collections.sort(onlyPrice);
		
		int max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println("Costilest Jacket Price : " + max);
		
		//driver.close();
		driver.findElementByClassName("brand-more").click();
		// Print Only Allen Solly Brand Minimum Price
		driver.findElementByXPath("//input[@value='Allen Solly']/following-sibling::div").click();
		driver.findElementByXPath("//div[@class='FilterDirectory-titleBar']/span").click();
		
		// wait for some time
		Thread.sleep(3000);

		// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		List<Integer>	onlyPrice2 = new ArrayList<Integer>();

		for (WebElement productPrice : allenSollyPrices) {
			onlyPrice2.add(Integer.parseInt(productPrice.getText().replaceAll("\\D", "")));
		}

		// Get the minimum Price 
		
		Collections.sort(onlyPrice2);
		System.out.println("size : " + onlyPrice.size());
		// Loop to sysout all values.
		/*for (Integer integer : onlyPrice2) {
			System.out.println(integer);
		}*/
		int min = Collections.min(onlyPrice2);

		// Find the lowest priced Allen Solly
		System.out.println("Allen Solly Minimum price : " + min);
		
		// Close the driver
		driver.close();


	}

}