package projectday;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import wdMethods.SeMethods;

public class ZoomCar extends SeMethods{

	@BeforeClass
	public void dataSetup() {
		testCaseName="TC_002 Zoom Car";
		testCaseDescription="Book Zoom Car";
		author ="Karthick";
		category="Regression";

	}
	@Test
	public void bookZoomCar() {
		
		//		2. Open https://www.zoomcar.com/chennai Website
		startApp("chrome", "https://www.zoomcar.com/chennai");
		//			3. Click on the Start your wonderful journey link
		click(locateElement("linkText", "Start your wonderful journey"));
		//			4. In the Search page, Click on the any of the pick up point under POPULAR PICK-UP POINTS
		click(locateElement("xpath", "//div[@class='component-popular-locations']/div[2]"));
		//			5. Click on the Next button
		clickWithNoSnap(locateElement("xpath", "//button[text()='Next']"));
		//			6. Specify the Start Date as tomorrow Date
		click(locateElement("xpath","//div[@class='days']/div[2]"));
			// get the date
			String date = getText(locateElement("xpath", "//div[@class='days']/div[2]")).replaceAll("\\D", "");
		//			7. Click on the Next Button
		clickWithNoSnap(locateElement("xpath", "//button[text()='Next']"));
		//			8. Confirm the Start Date and Click on the Done button
			verifyPartialText(locateElement("xpath", "//div[@class='breadcrumb']/div[2]/div[2]/div"), date);
			clickWithNoSnap(locateElement("linkText", "DONE"));
		//			9. In the result page, capture the number of results displayed.
			List<WebElement> findElementsByClassName = driver.findElementsByClassName("price");
		    for (WebElement index : findElementsByClassName) {
		    	System.out.println(index.getText());
			}
		//			10. Find the highest value and report the brand name.
		//			11. click on the Book Now button for it.
		//			12. Close the Browser.

	}

}
