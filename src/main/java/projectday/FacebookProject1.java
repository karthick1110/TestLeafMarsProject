package projectday;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import wdMethods.SeMethods;

public class FacebookProject1 extends SeMethods{

	public String url = "https://www.facebook.com";
	@BeforeClass
	public void dataSetup() {
		testCaseName = "TC_001 Project Facebook";
		testCaseDescription = "Like Test Leaf Page";
		author = "Karthick";
		category = "Progression";
	}
	@Test()
	public void facebookPageActions(){
		startApp("chrome", url);
		
		// Enter User credentials and Login
		WebElement username = locateElement("id", "email");
		type(username, "*****");
		WebElement password = locateElement("id", "pass");
		type(password, "Sathiya@35");
		click(locateElement("xpath", "//input[@type='submit']"));
		//6. In the home page Search box, enter the Text "TestLeaf"
		type(locateElement("xpath", "//input[@aria-label='Search']"), "TestLeaf");
		//7. Click on the search icon next to the Search textbox.
		click(locateElement("xpath", "//button[@aria-label='Search']"));
		//8. In the result window, Under the Places, Verify whether the Testleaf is displayed.
		verifyExactText(locateElement("xpath", "//span[text()='Pages']/../../following-sibling::div/div/div/div/a/div"), "TestLeaf");
		//9. Capture the text of the Like button.
		String text = getText(locateElement("xpath", "(//button[contains(@class,'PageLikeButton')])[1]"));
		System.out.println(text);
		//10. If the text is "Like" then click the button
		if(text.equals("Like")) {
			System.out.println("About to Click Like button");
			click(locateElement("xpath", "(//button[contains(@class,'PageLikeButton')])[1]"));
		}else {
			//11. If the text is "Liked" then report it is already liked.
			System.out.println("its already Liked");
		}
		//12. Click on the Link TestLeaf
		click(locateElement("xpath", "//span[text()='Places']/../../following-sibling::div/div/div/div/a/div"));
		//		13. Verify whether the Title contains the text Testleaf
		verifyTitle("TestLeaf");
		//		14. Capture the number of Likes for the page.
		String text2 = getText(locateElement("xpath", "(//span[text()='Community'])[2]/../following-sibling::div[2]/div/div/div"));
		//(//span[text()='Community'])[2]/../following-sibling::div[2]/div/div/div
		String Likes = text2.replaceAll("\\D", "");
		System.out.println(Likes);
	}
}
